# README #
This class writes log messages to the file system. The messages are included in the log files in the order in which they were passed to the Logger. It uses a singleton patter to ensure that a JVM will only have one instance of the Logger running at a time.

The log files will be written to the directory that the JVM is running from. If a different directory is required for the log files, the parentPath variable in createNewFile() should be edited

A new file for the logs to be written to will be created every fifteen minutes.

The logs can be read by other Java applications using a BufferedReader with a FileReader.
The filenames of the log files contain the data of creation of the file, so the files can easily be listed in creation order.
Any application that wanted to read the log files would then loop over the log files and read the contents using
    BufferedReader reader = new BufferedReader(new FileReader(logFileName));

reader.readLine() allows the lines in the file to be read one by one.

The lines in the file alternate between containing a timestamp (the time and date the message on the subsequent line was passed to the Logger), and the message that was passed to the Logger.

If the time of each logged message is required, the line containing the date can be converted to a Date object by using a SimpleDateFormatter with the same format string as is used when writing the log, and calling the parse() method on the line that has been read in.

The message itself will need no further processing to match the message that was originally passed to the logger.