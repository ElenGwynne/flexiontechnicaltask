import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * This class writes log messages to the file system. The messages are included in the log files in the order in which
 * they were passed to the Logger. It uses a singleton patter to ensure that a JVM will only have one instance of the
 * Logger running at a time.
 * The log files will be written to the directory that the JVM is running from. If a different directory is required for
 * the log files, the parentPath variable in createNewFile() should be edited
 *
 * A new file for the logs to be written to will be created every fifteen minutes.
 *
 *
 * The logs can be read by other Java applications using a BufferedReader with a FileReader.
 *
 * The filenames of the log files contain the data of creation of the file, so the files can easily be listed in
 * creation order.
 * Any application that wanted to read the log files would then loop over the log files and read the contents
 * using
 * BufferedReader reader = new BufferedReader(new FileReader(logFileName));
 *
 * reader.readLine() allows the lines in the file to be read one by one.
 *
 * The lines in the file alternate between containing a timestamp (the time and date the message on the subsequent line
 * was passed to the Logger), and the message that was passed to the Logger.
 *
 * If the time of each logged message is required, the line containing the date can be converted to a Date object by
 * using a SimpleDateFormatter with the same format string as is used when writing the log, and calling the parse()
 * method on the line that has been read in.
 *
 * The message itself will need no further processing to match the message that was originally passed to the logger.
 */
public class Logger
{

    private static Logger theInstance;

    private boolean writeFiles = true;
    private BufferedWriter logWriter;
    private ConcurrentLinkedQueue<DatedMessage> logs = new ConcurrentLinkedQueue<>();

    private final int fifteenMinutes = 15 * 60 * 1000; // 15 minutes in milliseconds

    private Logger()
    {
        swapFiles(); // do the initial file set up
        Thread fileSwapperThread = new Thread()
        {
            public void run()
            {
                while (!Thread.currentThread().isInterrupted())
                {
                    try
                    {
                        Thread.sleep(fifteenMinutes);
                        swapFiles();
                    }
                    catch (InterruptedException e)
                    {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread fileWritingThread = new Thread()
        {
            public void run()
            {
                while (writeFiles && !Thread.currentThread().isInterrupted())
                {
                    writeLogToFile(logs.poll());
                }
            }
        };

        fileSwapperThread.start();
        fileWritingThread.start();
    }

    public synchronized static Logger getInstance()
    {
        if (theInstance == null)
        {
            theInstance = new Logger();
        }
        return theInstance;
    }

    public void log(String message)
    {
        logs.add(new DatedMessage(message));
    }

    private void writeLogToFile(DatedMessage message)
    {
        if (message != null && logWriter != null && !message.getMessage().isEmpty())
        {
            try
            {
                logWriter.write(message.getDateString());
                logWriter.newLine();
                logWriter.write(message.getMessage());
                logWriter.newLine();
                logWriter.flush();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void swapFiles()
    {
        File newLogFile = createNewFile();

        if (newLogFile != null)
        {
            try
            {
                // specify a character set on the OutputStreamWriter for consistency across platforms
                OutputStreamWriter utfWriter = new OutputStreamWriter(new FileOutputStream(newLogFile), "UTF-8");
                BufferedWriter newWriter = new BufferedWriter(utfWriter);

                BufferedWriter oldLogWriter = logWriter;
                logWriter = newWriter;

                if (oldLogWriter != null)
                {
                    oldLogWriter.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private File createNewFile()
    {
        String dateString = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
        String logFileName = "Log_" + dateString;
        String parentPath = System.getProperty("user.dir"); // if the files are to be saved elsewhere, alter this line
        File newLogFile = new File(parentPath, logFileName + ".txt");

        int count = 1;
        while (newLogFile.exists()) // if the file already exists append a number in parentheses
        {
            newLogFile = new File(parentPath, logFileName + "_(" + count + ").txt");
            count++;
        }
        boolean fileCreated = false;

        try
        {
            fileCreated = newLogFile.createNewFile();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return fileCreated ? newLogFile : null; // if file creation failed return null
    }

    /**
     * A dispose method to safely close the BufferedWriter and clear the Logger instance
     */
    public void dispose()
    {
        writeFiles = false;
        try
        {
            logWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            logWriter = null;
            theInstance = null;
        }
    }

    /**
     * A simple class to attach a date to a message
     */
    private static class DatedMessage
    {
        private final Date date;
        private final String msg;

        DatedMessage(String s)
        {
            date = new Date();
            msg = s;
        }

        public String getDateString()
        {
            return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(date);
        }

        public String getMessage()
        {
            return msg;
        }
    }

}
